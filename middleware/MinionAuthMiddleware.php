<?php

namespace Middleware;

use Carbon\Carbon;
use Model\Minion;
use Model\User;
use Psr\Http\Server\RequestHandlerInterface;
use Slim\Psr7\Request;
use Slim\Psr7\Response;

class MinionAuthMiddleware
{
    public function __invoke(Request $request, RequestHandlerInterface $handler)
    {
        $response = new Response();
        $token = $_SERVER['HEADERS']['token'];
        if (empty($token)) {
            return \Service\Response::withJson($response, [
                'error' => [
                    'code' => 401,
                    'message' => 'Необходима авторизация'
                ]
            ], 401);
        }
        $minion = Minion::where('token', $token)
            ->whereDate('expired_at', '>=', Carbon::now())
            ->first();

        if(empty($minion))
            return \Service\Response::withJson($response, [
                'error' => [
                    'code' => 401,
                    'message' => 'Необходима авторизация'
                ]
            ], 401);
        $request = $request->withAttribute('minion', $minion);
        return $handler->handle($request);
    }

}