<?php

namespace Middleware;

use Model\User;
use Psr\Http\Server\RequestHandlerInterface;
use Slim\Psr7\Request;
use Slim\Psr7\Response;

class AuthMiddleware {
    public function __invoke(Request $request, RequestHandlerInterface $handler)
    {
        $response = new Response();
        if (empty($_SERVER['HEADERS']['token'])) {
            return \Service\Response::withJson($response, [
                'error' => [
                    'code' => 401,
                    'message' => 'Необходима авторизация'
                ]
            ], 401);
        }
        $token = $_SERVER['HEADERS']['token'];
        $user = User::whereHas('tokens', function($query) use ($token) {
            $query->where('token', $token);
        })->first();

        if(empty($user))
            return \Service\Response::withJson($response, [
                'error' => [
                    'code' => 401,
                    'message' => 'Необходима авторизация'
                ]
            ], 401);
        $request = $request->withAttribute('user', $user);
        $response = $handler->handle($request);
        return $response;
    }
}