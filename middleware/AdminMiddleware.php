<?php

namespace Middleware;

use Model\User;
use Psr\Http\Server\RequestHandlerInterface;
use Slim\Psr7\Request;
use Slim\Psr7\Response;

class AdminMiddleware {
    public function __invoke(Request $request, RequestHandlerInterface $handler)
    {
        $response = new Response();
        $user = $request->getAttribute('user');
        if(!($user->roles()->where('role_id', 1)->first()))
            return \Service\Response::withJson($response, [
                'error' => [
                    'code' => 403,
                    'message' => 'Доступ запрещен'
                ]
            ], 403);
        $response = $handler->handle($request);
        return $response;
    }
}