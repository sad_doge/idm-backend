<?php
ini_set('memory_limit', -1);
ini_set('post_max_size', '64M');
ini_set( 'upload_max_size' , '64M' );
use DI\Definition\Source\DefinitionArray;

use Illuminate\Database\Capsule\Manager as Capsule;
use Illuminate\Database\ConnectionResolver;
use Illuminate\Filesystem\Filesystem;
use Illuminate\Translation\FileLoader;
use Illuminate\Translation\Translator;
use Illuminate\Validation\DatabasePresenceVerifier;
use Middleware\TrailingSlash;
use Service\Logger;
use Service\Response;
use Slim\Factory\AppFactory;

$capsule = new Capsule;
$capsule->addConnection([
    'driver' => 'mysql',
    'host' => 'mysql',
    'database' => $_ENV['MYSQL_DATABASE'],
    'username' => $_ENV['MYSQL_USER'],
    'password' => $_ENV['MYSQL_PASSWORD'],
    'charset' => 'utf8',
    'collation' => 'utf8_unicode_ci',
    'prefix' => '',
]);
$capsule->setAsGlobal();
$capsule->bootEloquent();

$fileLoads = new FileLoader((new Filesystem()), __DIR__.'/locale');
$translator = new Translator($fileLoads, 'ru');
$validatorObj = new \Illuminate\Validation\Validator($translator, [], []);
$connectionResolver = new ConnectionResolver(['default'=>Capsule::connection()]);
$connectionResolver->setDefaultConnection('default');
$presenceVerifier = new DatabasePresenceVerifier($connectionResolver);
$validatorObj->setPresenceVerifier($presenceVerifier);

$validator = function($container) {
    $validator = $container->get('validatorObj');
    return function(array $data, array $rules) use ($validator) {
        $validator->setData($data);
        $validator->setRules($rules);
        return $validator->validate();
    };
};

$config = new DefinitionArray(
    [
        'settings' => [
            'displayErrorDetails' => false,
            'outputBuffering' => false,
        ],
        'validatorObj' => $validatorObj,
        'validator' => $validator,
    ]
);
$phpDI = new \DI\Container($config);

AppFactory::setContainer($phpDI);
//$app = AppFactory::create();
//
//
//
//$errHandler = function($request, $exception) use ($app) {
//    Logger::getLogger()->warning($exception->getMessage(). $exception->getFile());
//    $response = $app->getResponseFactory()->createResponse();
//    return Response::withJson($response, [
//        'status' => 'error',
//        'error' => $exception->getMessage(),
//        'file' => $exception->getFile(),
//        'line' => $exception->getLine(),
//    ], 500);
//};
//
//
//$errorMiddleware = $app->addErrorMiddleware(false, false, false);
//$errorMiddleware->setDefaultErrorHandler($errHandler);
//$app->add(TrailingSlash::class);
//foreach (glob(__DIR__."/routes/*.php") as $filename)
//{
//    include $filename;
//}