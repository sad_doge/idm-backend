<?php

use Illuminate\Validation\ValidationException;
use Model\Agent;
use Service\Response;

$app->group('/api', function ($app) {
    $app->post('/agents', function ($request, $response, $args) {
        $data = json_decode($_POST, true) ?? [];
        $validator = $this->get('validator');
        $rules =  [
            'client_id' => 'numeric',
            'ip' => 'required|ip|unique:' . Agent::class,
        ];
        try {
            $validator($data, $rules);

            $result = Agent::create($data);
            return Response::withJson($response, [
                'code' => 201,
                'message' => 'Agent created',
                'data' => $result,
            ], 201);

        } catch (ValidationException $e) {
            return Response::withJson($response, [
                'errors' => $e->errors(),
            ], 400);
        }
    });

    $app->get('/agents/{id:\d+}',function ($request, $response, $args) {
        $result = Agent::find($args['id']);
        if(empty($result))
            return Response::withJson($response, [
                'error' => [
                    'code' => 404,
                    'message' => 'Agent not found'
                ]
            ], 404);

        return Response::withJson($response, $result, 200);
    });

    $app->put('/agents/{id:\d+}',function ($request, $response, $args) {
        $result = Agent::find($args['id']);
        if(empty($result))
            return Response::withJson($response, [
                'error' => [
                    'code' => 404,
                    'message' => 'Agent not found'
                ]
            ], 404);
        $data = json_decode($_POST, true) ?? [];
        $validator = $this->get('validator');
        $rules =  [
            'client_id' => 'numeric',
            'ip' => 'required|ip|unique:' . Agent::class . ',ip,' .$result->id,
        ];
        try {
            $validator($data, $rules);
            $result = $result->update($data);
            return Response::withJson($response, [
                'code' => 201,
                'message' => 'Agent updated',
                'data' => $result,
            ], 201);

        } catch (ValidationException $e) {
            return Response::withJson($response, [
                'errors' => $e->errors(),
            ], 400);
        }
    });

    $app->delete('/agents/{id:\d+}',function ($request, $response, $args) {
        $result = Agent::find($args['id']);
        if(empty($result))
            return Response::withJson($response, [
                'error' => [
                    'code' => 404,
                    'message' => 'Agent not found'
                ]
            ], 404);
        $result->delete();
        return Response::withJson($response, [
            'status' => [
                'code' => 202,
                'message' => 'Agent deleted'
            ]
        ],202);
    });

    $app->get('/agents',function ($request, $response, $args) {
        return Response::withJson($response, Agent::with([])->get(), 200);
    });

    $app->get('/agents/{offset:\d+}/{count:\d+}', function ($request, $response, $args) {
        $result = [
            'count' => Agent::count(),
            'data'  => Agent::with([])
                ->skip($args['offset'])
                ->take($args['count'])
                ->get(),
        ];
        return Response::withJson($response, $result, 200);
    });
})->add(\Middleware\MinionAuthMiddleware::class);
