<?php


use Illuminate\Validation\ValidationException;
use Middleware\MinionAuthMiddleware;
use Service\Minions\MinionTrend;
use Service\Response;
use Service\SQLiteConnection;

$app->get('/asdf', function ($request, $response, $args) {
    $path = ROOT_PATH . '/sqlite/2022-03-28_10-27-01_trend.db';
    $pdo = (new SQLiteConnection())->connect($path);

    if($pdo) {
        $query = $pdo->query("SELECT * FROM trend_info");
        $trends_info = $query->fetchAll(\PDO::FETCH_ASSOC);
        $query = $pdo->query("SELECT * FROM trend_data_1");
        $records = $query->fetchAll(\PDO::FETCH_ASSOC);
        $result = collect();

        foreach ($records as $record) {

//            foreach ($trends_info as $trend) {
                $kek = [
                    'date' => $record['Time_Stamp'],
                    'value' => $record[$trends_info[0]['Trend_Id'] . '_avg'],
                    'name' => $trends_info[0]['Name'],
                ];
//                $kek['value'] = $record[$trend['Trend_Id'] . '_avg'];
//                $kek['name'] = $trend['Name'];
//                var_dump();
                $result->add($kek);
//            }
        }

//        var_dump($query->fetchAll(\PDO::FETCH_ASSOC));
        return Response::withJson($response, [
            'error' => [
                'code' => 200,
                'message' => '-_-',
                'asdf' => $result->toArray(),
            ]
        ], 200);
    }
    else
    return Response::withJson($response, [
        'error' => [
            'code' => 400,
            'message' => 'Hello, there!',
        ]
    ], 400);
}); //todo remove

$app->post('/api/send-data' , function ($request, $response, $args) {
    $data = $_FILES;
    $validator = $this->get('validator');
    $rules =  [
        'file' => 'required',
    ];
    try {
        $validator($data, $rules);
        $path = ROOT_PATH . '/public/' . $data['file']['name'];
//        if(file_exists($path)) {
//            return Response::withJson($response, [
//                'errors' => 'the db file already exist',
//            ], 400);
//        }
        move_uploaded_file($data['file']['tmp_name'], $path);

//        $type = $request->getAttribute('minion')->type_id;
        $minion_process = new MinionTrend($request->getAttribute('minion')->id, $path);

        $result = $minion_process->execute();

        return Response::withJson($response, [
            'result' => $result
        ], 200);

    } catch (ValidationException $e) {
        return Response::withJson($response, [
            'errors' => $e->errors(),
        ], 400);
    }
})->add(MinionAuthMiddleware::class);

$app->get('/api/get-last', function ($request, $response, $args) {
    $result = \Model\Trend::query()->where('minion_id', $request->getAttribute('minion')->id)
        ->orderBy('date', 'desc')
        ->first();

    return Response::withJson($response, [
        'data' => $result,
    ], 200);
})->add(MinionAuthMiddleware::class);

$app->post('/my-ip', function ($request, $response, $args) {
    return Response::withJson($response, [
        'my-ip' => $_SERVER['REMOTE_ADDR'],
        'root_dir' => 'kekw!!!',
        'agents' =>     \Model\Trend::truncate(),
    ], 200);
})->add(MinionAuthMiddleware::class); // todo remove