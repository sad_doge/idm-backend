<?php

use Illuminate\Validation\Rule;
use Illuminate\Validation\ValidationException;
use Middleware\MinionAuthMiddleware;
use Service\Response;

$app->group('/api', function ($app) {
    $app->post('/minions', function ($request, $response, $args) {
        $data = json_decode($_POST, true) ?? [];
        $validator = $this->get('validator');
        $rules =  [
            'type_id' => 'required|numeric',
            'agent_id' => [
                'required',
                Rule::unique('minions')->where(function ($q) use ($data) {
                    return $q->where('type_id', $data['type_id'])->where('agent_id', $data['agent_id']);
                }),
            ],

        ];
        try {
            $validator($data, $rules);

//            $result = Agent::create($data);
            return Response::withJson($response, [
                'code' => 201,
                'message' => 'Agent created',
                'data' => 'kekw',
            ], 201);

        } catch (ValidationException $e) {
            return Response::withJson($response, [
                'errors' => $e->errors(),
            ], 400);
        }
    });



})->add(MinionAuthMiddleware::class);