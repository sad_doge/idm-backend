<?php


//use Middleware\AuthMiddleware;
use Illuminate\Validation\ValidationException;
use Model\UserToken;
use Service\Logger;
use Service\Response;
use Model\User;

$app->post('/api/user/signin', function ($request, $response, $args) {
    $data = json_decode($_POST, true);
    $data = $data['user'];

    $user = User::with('tokens')->where('login', $data['login'])->first();

    if ($user) {
        if (password_verify($data['password'], $user->password)) {
            $user->tokens()->delete();
            $newToken = new UserToken(['user_id' => $user->id, 'token' => md5(uniqid($user->id, true)), 'expired_at' => date('Y-m-d H:i:s', strtotime("+5 hour"))]);
            $newToken->save();

            $user = User::with(['tokens'])->find($user->id);

            Logger::getLogger()->info($user->login . ' залогинен');

            return Response::withJson($response, $user, 200);
        }
    }

    return Response::withJson($response, [
        'error' => [
            'code' => 403,
            'message' => 'Неверный логин или пароль'
        ]
    ], 403);
});

$app->post('/api/singin', function ($request, $response, $args) {
    $data = json_decode($_POST, true) ?? [];
    $rules = [
        'type' => 'required',
    ];

    $validator = $this->get('validator');
    try {
        $validator($data, $rules);
        $minion = \Model\Minion::where('type_id', $data['type'])
//            ->whereHas('agent', function ($q) { //todo that's right
//                $q->where('ip', $_SERVER['REMOTE_ADDR']);
//            })
            ->first();
        if ($minion) {
            $token = md5(uniqid($minion->id, true));
            $minion->update([
                'token' => $token,
                'expired_at' => date('Y-m-d H:i:s', strtotime("+24 hour")),
            ]);
            return Response::withJson($response, [
                'token' => $token,
                'data'  => $minion,
            ], 200);
        }
        else {
            return Response::withJson($response, [
                'errors' => 'Your server does not have a minion with the type',
            ], 403);
        }
    } catch (ValidationException $e) {
    return Response::withJson($response, [
        'errors' => $e->errors(),
    ], 400);
}
});

//$app->get('/api/user/signout', function ($request, $response, $args) {
//    $user = $request->getAttribute('user');
//
//    if ($user) {
//        $user->tokens()->delete();
//    }
//
//    Logger::getLogger()->info($user->login . ' разлогинен');
//
//    return Response::withJson($response, [
//        'error' => [
//            'code' => 200,
//            'message' => 'Успешный выход'
//        ]
//    ], 403);
//})->add(AuthMiddleware::class);;

