<?php

namespace Factories;

use Service\Logger;
use Service\Response;

class SlimApp
{
    static $app = null;

    static function get()
    {
        return is_null(self::$app) ? self::init() : self::$app;
    }

    static function init()
    {
        self::$app = \Slim\Factory\AppFactory::create();
        $app = &self::$app;
        $errHandler = function ($request, $exception) use ($app){
            $response = self::$app->getResponseFactory()->createResponse();
            error_log($exception->getMessage());
            if ($exception->getCode() === 404) {
                return Response::withJson($response, [
                    'status' => 'Error',
                    'message' => 'Not found',
                ], 404);
            } else {
                Logger::getLogger()->warning($exception->getMessage(). $exception->getFile());
                return Response::withJson($response, [
                    'status' => 'Error',
                    'message' => $exception->getMessage(),
                    'file' => $exception->getFile(),
                    'line' => $exception->getLine(),
                ], 500);
            }
        };
        self::$app->addErrorMiddleware(false, false, false)->setDefaultErrorHandler($errHandler);

        foreach (glob(ROOT_PATH . '/routes/*.php') as $file) {
            require $file;
        }
        unset($app);
        return self::$app;
    }
}