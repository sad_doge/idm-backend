create table trends
(
    id bigint auto_increment primary key,
    minion_id bigint NOT NULL,
    date bigint NOT NULL,
    value float NULL,
--     data JSON NOT NULL,
    name varchar(255) NOT NULL,
    type varchar (10) NOT NULL,
    warning float NULL,
    alarm float NULL,
    created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP ,
    updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    CONSTRAINT trends_date_name_type_unique UNIQUE (date, name, type),
    constraint trends_minions_id_fk
        foreign key (minion_id) references minions (id)
        on update cascade on delete cascade
);
-- INSERT INTO users (id, first_name, middle_name, last_name, login, email, password, is_active) VALUES (5, 'UserKEK5W', 'User1', 'User51', 'use5r', 'user@5mail.ru', '$2y$10$czMGR/JvoCueD7fhTh081.OTry0d8S1f5PJ.nzulysx.7pPH.a/KC', 1);

-- INSERT INTO users (id, first_name, middle_name, last_name, login, email, password, is_active) VALUES (14, 'User', 'User1', 'User1', 'user', 'user@mail.ru', '$2y$10$czMGR/JvoCueD7fhTh081.OTry0d8S1f5PJ.nzulysx.7pPH.a/KC', 1);
-- INSERT INTO users (id, first_name, middle_name, last_name, login, email, password, created_at, updated_at, is_active) VALUES (13, 'Admin', 'Admin', 'Admin', 'admin', 'admin@mail.ru', '$2y$10$czMGR/JvoCueD7fhTh081.OTry0d8S1f5PJ.nzulysx.7pPH.a/KC', '2022-01-25 18:07:30', '2022-01-25 18:07:30', 1);