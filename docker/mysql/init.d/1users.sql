create table users
(
    id          int auto_increment
        primary key,
    first_name  varchar(255) null,
    middle_name varchar(255) null,
    last_name   varchar(255) null,
    login       varchar(255) null,
    email       varchar(255) null,
    password    varchar(255) null,
    created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP ,
    updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
);

INSERT INTO users (id, first_name, middle_name, last_name, login, email, password) VALUES (45, 'UserKEKW', 'User1', 'User1', 'user', 'user@mail.ru', '$2y$10$czMGR/JvoCueD7fhTh081.OTry0d8S1f5PJ.nzulysx.7pPH.a/KC', 1);
INSERT INTO users (id, first_name, middle_name, last_name, login, email, password, created_at, updated_at) VALUES (13, 'Admin', 'Admin', 'Admin', 'admin', 'admin@mail.ru', '$2y$10$czMGR/JvoCueD7fhTh081.OTry0d8S1f5PJ.nzulysx.7pPH.a/KC', '2022-01-25 18:07:30', '2022-01-25 18:07:30', 1);
