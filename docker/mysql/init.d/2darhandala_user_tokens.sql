create table user_tokens
(
    id         int auto_increment
        primary key,
    user_id    int          null,
    token      varchar(255) null,
    expired_at timestamp    null,
    created_at timestamp    null,
    updated_at varchar(255) null,
    constraint user_tokens_users_id_fk
        foreign key (user_id) references users (id)
            on update cascade on delete cascade
);
