
create table minions
(
    id bigint auto_increment primary key,
    type_id int NULL, -- TODO add foreign key --
    agent_id bigint NOT NULL,
    token varchar(255) NULL,
    expired_at TIMESTAMP NULL,
    created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP ,
    updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    constraint minions_agents_id_fk
        foreign key (agent_id) references agents (id)
            on update cascade on delete cascade
);

-- create table minions
-- (
--     id int auto_increment primary key,
--     type_id bigint NULL, -- TODO add foreign key --
--     agent_id bigint NOT NULL,
--     token varchar(255) NULL,
--
--     created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP ,
--     updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
--     FOREIGN KEY (agent_id)  REFERENCES agents (id)
-- );

-- INSERT INTO users (id, first_name, middle_name, last_name, login, email, password, is_active) VALUES (4, 'UserKE4KW', 'User1', 'User1', 'us4er', 'user@4mail.ru', '$2y$10$czMGR/JvoCueD7fhTh081.OTry0d8S1f5PJ.nzulysx.7pPH.a/KC', 1);

INSERT INTO minions (type_id, agent_id) VALUES (1, 1);

-- INSERT INTO users (id, first_name, middle_name, last_name, login, email, password, is_active) VALUES (14, 'User', 'User1', 'User1', 'user', 'user@mail.ru', '$2y$10$czMGR/JvoCueD7fhTh081.OTry0d8S1f5PJ.nzulysx.7pPH.a/KC', 1);
-- INSERT INTO users (id, first_name, middle_name, last_name, login, email, password, created_at, updated_at, is_active) VALUES (13, 'Admin', 'Admin', 'Admin', 'admin', 'admin@mail.ru', '$2y$10$czMGR/JvoCueD7fhTh081.OTry0d8S1f5PJ.nzulysx.7pPH.a/KC', '2022-01-25 18:07:30', '2022-01-25 18:07:30', 1);