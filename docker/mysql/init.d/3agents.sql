

-- create table agents
-- (
--     id          int auto_increment
--         primary key,
--     first_name  varchar(255) null,
--     ip varchar(15) NOT NULL,
--     created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP ,
--     updated_at DATETIME DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
-- );

create table agents
(
    id bigint auto_increment primary key,
    client_id bigint NULL, -- TODO add foreign key --
    ip varchar(15) NOT NULL,
    created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP ,
    updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    CONSTRAINT agents_ip_unique UNIQUE (ip)
);
--
-- INSERT INTO users (id, first_name, middle_name, last_name, login, email, password, is_active) VALUES (3, 'UserKEKW3', 'User31', 'User1', 'user', 'user@mai3l.ru', '$2y$10$czMGR/JvoCueD7fhTh081.OTry0d8S1f5PJ.nzulysx.7pPH.a/KC', 1);
--
INSERT INTO agents (client_id, ip) VALUES (1, '172.25.0.1');

-- INSERT INTO users (id, first_name, middle_name, last_name, login, email, password, is_active) VALUES (14, 'User', 'User1', 'User1', 'user', 'user@mail.ru', '$2y$10$czMGR/JvoCueD7fhTh081.OTry0d8S1f5PJ.nzulysx.7pPH.a/KC', 1);
-- INSERT INTO users (id, first_name, middle_name, last_name, login, email, password, created_at, updated_at, is_active) VALUES (13, 'Admin', 'Admin', 'Admin', 'admin', 'admin@mail.ru', '$2y$10$czMGR/JvoCueD7fhTh081.OTry0d8S1f5PJ.nzulysx.7pPH.a/KC', '2022-01-25 18:07:30', '2022-01-25 18:07:30', 1);