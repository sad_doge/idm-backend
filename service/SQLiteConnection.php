<?php

namespace Service;

class SQLiteConnection
{
    private $pdo;

    public function connect($path) {
        try {
            return $this->pdo = new \PDO("sqlite:" . $path);
        } catch (\PDOException $e) {

        }
    }
}