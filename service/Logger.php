<?php
namespace Service;

use Logger\Formatter\ConsoleFormatter;
use Monolog\Handler\RotatingFileHandler;
use Monolog\Handler\StreamHandler;
use Monolog\Logger as MonologLogger;
class Logger {
    static $logger;

    static function getLogger() {
        if(is_null(self::$logger)) {
            $logger = new \Monolog\Logger('my_logger');
            foreach(MonologLogger::getLevels() as $levelName => $levelCode) {
                $fileHandler = new RotatingFileHandler('/var/log/app/app-'.strtolower($levelName).'.log', 0, $levelCode, false);
                $logger->pushHandler($fileHandler);
                $consoleHandler = new StreamHandler("php://stdout", $levelCode, true);
                $consoleHandler->setFormatter(new ConsoleFormatter());
                $logger->pushHandler($consoleHandler);
            }
            self::$logger = $logger;
        }
        return self::$logger;
    }
}