<?php

namespace Service\Minions;

interface BaseMinion
{
    public function execute();

    public function result();
}