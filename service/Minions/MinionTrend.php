<?php

namespace Service\Minions;

use Illuminate\Database\Capsule\Manager as DB;
use Model\Trend;
use Service\SQLiteConnection;

class MinionTrend implements BaseMinion
{
    private $minion_id;
    private $path;

    public function __construct($minion_id, $path) {
        $this->minion_id = $minion_id;
        $this->path = $path;
    }

    public function execute()
    {
        $pdo = (new SQLiteConnection())->connect($this->path);

        $query = $pdo->query("SELECT * FROM trend_info");
        $trends_info = $query->fetchAll(\PDO::FETCH_ASSOC);

        $query = $pdo->query("SELECT *  FROM trend_data_1 ORDER BY Time_Stamp");
        $trends_data = $query->fetchAll(\PDO::FETCH_ASSOC);

        $start_time = $trends_data[0]['Time_Stamp'];
        $end_time = end($trends_data)['Time_Stamp'];

        $query = $pdo->query("SELECT *  FROM limits_journal WHERE Time_From <= " . $start_time . " and Time_To >= " . $end_time);
        $trends_limits = ($query->fetchAll(\PDO::FETCH_ASSOC));



        DB::beginTransaction();
        try {
            $insert = array();
            foreach ($trends_data as $data) {
                $limit = collect($trends_limits)->where('Time_From', '<=', $data['Time_Stamp'])
                    ->where('Time_To', '>=', $data['Time_Stamp'])
                    ->first();

                foreach (['avg'] as $type) {
                    foreach ($trends_info as $trend) {
                        $insert[] = [
                            'date' => $data['Time_Stamp'],
                            'minion_id' => $this->minion_id,
                            'value' => $data[$trend['Trend_Id'] . '_' . $type],
                            'name' => $trend['Name'],
                            'warning' => $limit[$trend['Trend_Id'] . '_warn'] ?? null,
                            'alarm' => $limit[$trend['Trend_Id'] . '_alarm'] ?? null,
                            'type' => $type,
                        ];
                    }
                }
            }

//            $query = "INSERT INTO trends (date, minion_id, value, name, warning, alarm, type)
//                       VALUES (:date, :minion_id, :value, :name, :warning, :alarm, :type)";

            foreach (array_chunk($insert, 1700) as $chunk)
            {
                Trend::insert($chunk);
            }
            DB::commit();

            return end($insert);
        } catch (\Exception $e) {
            DB::rollBack();
            error_log($e);
            return $e;
        }
    }

    public function result()
    {
        // TODO: Implement result() method.
    }
}