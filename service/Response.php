<?php

namespace Service;

class Response {
    public static function withJson($response, $json, $status) {
        $response->getBody()->write(json_encode($json));
        return $response
            ->withHeader('Content-Type', 'application/json')
            ->withStatus($status);
    }
}