<?php

namespace Service;

class Cache {
    protected static $memcached;

    public static function getCache() {
        if(is_null(self::$memcached)) {
            $memcached = new \Memcached();
            $memcached->addServer('mysql', 11211);
            self::$memcached = $memcached;
        }
        return self::$memcached;
    }
}