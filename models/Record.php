<?php

namespace Model;

use Illuminate\Database\Eloquent\Model;

class Record extends Model
{
    protected $table = 'records';
    protected $fillable = [
        'id',
        'date',
        'value',
        'idm_server_id',
        'name',
        'speed',
    ];


}