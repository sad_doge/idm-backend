<?php

namespace Model;

use Illuminate\Database\Eloquent\Model;

class Minion extends Model
{
    protected $table = 'minions';
    protected $fillable = [
        'type_id',
        'agent_id',
        'token',
        'expired_at',
    ];

    public function agent() {
        return $this->belongsTo(Agent::class);
    }
}