<?php
namespace Model;

use Illuminate\Database\Eloquent\Model;

class User extends Model {
    protected $table = 'users';
    protected $fillable = [
        'first_name',
        'last_name',
        'middle_name',

    ];

    protected $hidden = [
        'password'
    ];

    public function setPasswordAttribute($password) {
        $this->attributes['password'] = password_hash($password, PASSWORD_BCRYPT);
    }

    public function roles() {
        return $this->belongsToMany(Role::class, 'user_roles');
    }

    public function tokens() {
        return $this->hasMany(UserToken::class);
    }

}