<?php

namespace Model;

use Illuminate\Database\Eloquent\Model;

class UserToken extends Model {
    protected $fillable = ['user_id','token', 'expired_at'];
}