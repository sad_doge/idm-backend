<?php

namespace Model;

use Illuminate\Database\Eloquent\Model;

class Trend extends Model
{
    protected $table = 'trends';
    protected $fillable = [
        'id',
        'date',
        'value',
        'minion_id',
        'data',
    ];

    protected $casts = [
        'data' => 'array'
    ];
}