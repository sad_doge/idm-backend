<?php

namespace Model;

use Illuminate\Database\Eloquent\Model;

class Agent extends Model
{
//    protected $table = 'agents';
    protected $fillable = [
        'client_id',
        'ip',
    ];

    public function minions() {
        return $this->hasMany(Minion::class);
    }
}