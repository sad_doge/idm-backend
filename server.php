<?php

use Factories\SlimApp;
use Slim\Factory\ServerRequestCreatorFactory;
use Swoole\Http\Server;
use Swoole\Http\Request;
use Swoole\Http\Response;
use Swoole\Coroutine\Http\Client;
use Slim\App;

//date_default_timezone_set(getenv("TZ"));
require_once __DIR__ . '/vendor/autoload.php';

require 'bootstrap.php';
/**
 * @var App $app
 * @var DI\Container $phpDI
 */
$server_port = 8000;
const ROOT_PATH = __DIR__;

//$process = new \Swoole\Process(function ($process) {
//    Co\Run(function() {
//        go(function() {
//            function clearExpiredTokens() {
//                Logger::getLogger()->info("Пошло удаление");
//                $expiredTokens = UserToken::where('expired_at', '<', date('Y-m-d H:i:s'))->delete();
//            }
//
//            Logger::getLogger()->info("Пошел запуск експайред делит токен");
//            \Swoole\Timer::tick(1 * 60 * 1000, "clearExpiredTokens");
//        });
//    });
//}, FALSE);
//
//$process->start();

$server = new Server("0.0.0.0", intval($server_port));
$server->set([
    'buffer_output_size' => 32 * 1024*1024,
    'package_max_length' => 16 * 1024 * 1024,
]);

$app = SlimApp::get();

$server->on('request', function (Request $req, Response $res) use ($server, $app) {

    if ($_ENV['DEBUG']) {
        $app = SlimApp::init();
        $start_time = microtime(true);
    }

    $_SERVER['REQUEST_URI'] = $req->server['request_uri'];
    $_SERVER['REQUEST_METHOD'] = $req->server['request_method'];
    $_SERVER['REMOTE_ADDR'] = $req->server['remote_addr'];
    $_SERVER['HEADERS'] = $req->header;
    $_GET = $req->get ?? [];
    $_POST = $req->post ?? $req->rawContent();
    $_FILES = $req->files ?? [];
    $serverRequestCreator = ServerRequestCreatorFactory::create();
    $request = $serverRequestCreator->createServerRequestFromGlobals();
    $response = $app->handle($request);
    foreach ($response->getHeaders() as $key => $value) {
        if ($key !== 'Content-Length') {
            $res->header($key, $value[0]);
        }
    }
    $res->status($response->getStatusCode());
    $res->end($response->getBody());

    if ($_ENV['DEBUG']) {
        error_log('Duration: ' . round(microtime(true) - $start_time, 2)  . 's / CPU usage: ' . round(sys_getloadavg()[0], 3)  . '% / RAM usage: ' . round(memory_get_usage() / 1024 / 1024, 2) . 'MB');
//        $server->reload();
    }
});
$server->start();



















